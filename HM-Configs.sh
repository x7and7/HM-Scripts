#!/usr/bin/env bash
# _   _ __  __
#| | | |  \/  |
#| |_| | |\/| | Made by Haider Mirza
#|  _  | |  | | My Github: https://github.com/Ha1derMirza
#|_| |_|_|  |_|

# NOTE: dependencies: dunst, libnotify, dmenu.
# Dunst and Libnotify are optional if you remove line 24

options=( 'qute-conf'
          'qute-quick'
          'emacs-conf'
          'emacs-pkg'
          'emacs-init'
          'xmonad'
          'dunst'
          'xmobar'
          'alacritty'
          'conky'
          'xmobar'
          'quit' )
Choice=$(printf '%s\n' "${options[@]}" | dmenu -i -p 'Edit config:')
notify-send "Opening ${Choice}"

if [ "$Choice" = "quit" ]; then
echo "Program Terminated"
elif [ "$Choice" = "qute-conf" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/qutebrowser/config.py
elif [ "$Choice" = "qute-quick" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/qutebrowser/quickmarks
elif [ "$Choice" = "emacs-conf" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/doom/README.org
elif [ "$Choice" = "emacs-pkg" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/doom/packages.el
elif [ "$Choice" = "emacs-init" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/doom/init.el
elif [ "$Choice" = "xmonad" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.xmonad/README.org
elif [ "$Choice" = "dunst" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/dunst/dunstrc
elif [ "$Choice" = "xmobar" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/doom-one-xmobarrc
elif [ "$Choice" = "alacritty" ]; then
    exec emacsclient -c "$HOME"/repos/.dotfiles/.config/alacritty/alacritty.yml
elif [ "$Choice" = "conky" ]; then
  exec emacsclient -c "$HOME"/repos/.dotfiles/.config/conky/xmonad/doom-one-01.conkyrc
fi
